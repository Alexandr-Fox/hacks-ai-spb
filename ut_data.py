import numpy as np
import random as r
import pandas as pd
from tqdm import tqdm

INPUT_DATA_FILE = "not_simple.csv"
OUTPUT_DATA_DIRECTORY = "new_cut_data"
OUTPUT_SIZE = 40000


def main():
    main_data = pd.read_csv(INPUT_DATA_FILE)
    OUTPUT_SIZE = int(main_data.shape[0]//40)
    n = len(main_data) // OUTPUT_SIZE
    main_data = main_data.sample(frac=1, random_state=51)
    print(main_data.shape[0])
    print(len(main_data))
    for i in tqdm(range(n), desc="splitter"):

        if len(main_data) - OUTPUT_SIZE > 0:
            random_number = r.randint(0, len(main_data) - OUTPUT_SIZE)

            new_data = main_data.iloc[random_number:(random_number + OUTPUT_SIZE)]
            new_data.reset_index(drop=True, inplace=True)

            new_data.to_csv(f"{OUTPUT_DATA_DIRECTORY}/bit_data_{i}.csv")

            main_data = main_data.drop(index=[k for k in range(random_number, random_number + OUTPUT_SIZE)])
            main_data.reset_index(drop=True, inplace=True)
        else:
            break

    main_data.to_csv(f"{OUTPUT_DATA_DIRECTORY}/bit_data_{n}.csv")

    return 0


if __name__ == '__main__':
    main()
