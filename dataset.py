import pandas as pd
import itertools
import random
import time
from tqdm import tqdm


# pd.options.display.max_colwidth = -1
# pd.options.display.min_rows = 100


def load_dataset(path):
    df = pd.read_csv(path, sep=",", header=0)
    return df[["target_building_id", "target_address"]]


def create_pair_data(text, labels, size):
    data_dict = {}
    start = time.time()
    for x in pd.DataFrame({"text": text, "label": labels}).groupby("label").apply(
            lambda x: list(x["text"])).reset_index().values.tolist():
        data_dict[x[0]] = x[1]
    # data_dict = dict(itertools.islice(data_dict.items(), 75000, None))
    # print(len(data_dict.keys()))
    # label1 = []
    label1 = [pair for d in data_dict.keys() for pair in itertools.combinations(data_dict[d], 2)]
    # print("two")
    label0 = []
    for i, d1 in enumerate(data_dict.keys()):
        if len(label0) >= len(label1):
            break
        for d2 in list(data_dict.keys())[i+1:]:
            label0.extend(list(itertools.product(data_dict[d1], data_dict[d2])))
    """
    all_keys = list(data_dict.keys())    
    for d1, d2 in tqdm(itertools.product(all_keys, repeat=2), desc='Label 0', total=len(all_keys)**2):
        if d1 != d2:
            label0.extend([(t1, t2) for t1 in data_dict[d1] for t2 in data_dict[d2] if (d1, d2) not in [(k1, k2) for k1, k2 in itertools.combinations([d1, d2], 2)]])
        if len(label0) >= len(label1):
            break
    """
    print(time.time() - start)
    # label0 = random.sample(label0, len(label1))
    return label1, label0
    # return random.sample(label1, int(size / 2000)), random.sample(label0, int(size / 2000)).


def get_corpus(path):
    df = pd.read_csv(path, sep=",", header=0)
    df.drop_duplicates(subset=["full_address"], inplace=True)
    print(len(df))
    return df["id"].tolist(), df["full_address"].tolist()


def get_test_dataset(path):
    df = pd.read_csv(path, sep=",", header=0)
    return df["address"].tolist(), df["target_building_id"].tolist()
